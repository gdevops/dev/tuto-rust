
.. index::
   pair: Rust ; Installation

.. _rust_installation:

=====================
Rust installation
=====================

.. toctree::
   :maxdepth: 3

   rustup/rustup
