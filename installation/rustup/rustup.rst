
.. index::
   ! rustup
   pair: Rust ; Cargo
   ! Cargo

.. _rustup:

=====================
Rustup
=====================


.. seealso::

   - https://www.rust-lang.org/tools/install

.. contents::
   :depth: 3

On GNU/Linux
=============

.. literalinclude:: installation.txt
   :linenos:


Cargo: the Rust build tool and package manager
================================================

.. seealso:: :ref:`cargo`
