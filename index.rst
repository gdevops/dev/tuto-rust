
.. index::
   pair: Rust ; Language


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/dev/tuto-rust/rss.xml>`_

.. _rust_language:
.. _rust_tuto:

===========================
|rust| **Rust language**
===========================

- https://en.wikipedia.org/wiki/Rust_(programming_language)
- https://github.com/rust-lang/rust
- https://www.rust-lang.org/
- https://github.com/rust-unofficial/awesome-rust
- https://mastodon.social/@rust_discussions
- https://mastodon.social/@rust_discussions.rss

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
   installation/installation
   cargo/cargo
   news/news
   tools/tools
